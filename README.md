## Adam Knee's Portfolio

This portfolio was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Dev Stack

[Create React App](https://github.com/facebook/create-react-app)
[Firebase](https://firebase.google.com/)
[Express](https://expressjs.com/)

## Design Stack

Figma
Adobe Illustrator
Adobe Photoshop

## Important Notes:

- If you run the backend server, you need to add a `congif.js` file. I provided a sample for you.

- This was built over a long period of time during different points of React's lifespan. Due to this, you'll see some different React coding conventions all over the place. This started out primarily as a way for me to learn ReactJS.

- Some of the SCSS/CSS is sloppy, I acknowledge this. It wasn't until late in developing this that I discovered styled components and css modules

- Lastly, I'm aware that you can nest descending selectors in SCSS. I choose not to do this in most cases because it keeps the code cleaner in my opinion.

## Running the App

I doubt anyone is cloning my portfolio but if you are, here's the instructions:

In the frontend project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `npm run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify

## Running the backend node server (For form submission)

In the backend project directory, run `node server.js`
