// const express = require('express');
// const serverless = require('serverless-http');
// const nodemailer = require('nodemailer');
// const creds = require('./config');
// const app = express();
// const port = process.env.PORT || 5000;

// const transport = {
//   host: 'smtp.gmail.com',
//   auth: {
//     user: creds.USER,
//     pass: creds.PASS,
//   },
// };

// const transporter = nodemailer.createTransport(transport);

// app.listen(port, () => console.log(`Listening on port ${port}`));

// app.get('/api/express-backend', (req, res) => {
//   res.send({ express: 'connected' });
// });

// transporter.verify(err => {
//   if (err) {
//     console.log('Error starting transporter server: ', err);
//   } else {
//     console.log('Server started successfully.');
//   }
// });

// app.use(express.json());

// app.post('/send', (req, res, next) => {
//   const name = req.body.name;
//   const email = req.body.email;
//   const message = req.body.messageHtml;

//   const mail = {
//     from: email,
//     to: 'adam.l.knee@gmail.com',
//     subject: `Portfolio Site Contact Email: ${name}`,
//     html: `
//       <h2>Name: ${name}.</h2>
//       <h2>Email: ${email}.</h2>
//       <p><strong>Message:</strong> ${message}</p>
//       `,
//   };

//   transporter.sendMail(mail, err => {
//     res.json({
//       msg: err ? 'fail' : 'success',
//     });
//   });
// });

// module.exports.handler = serverless(app);
