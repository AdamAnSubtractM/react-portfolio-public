import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { firebaseApp, firebaseFirestore } from './js/firebase';
import { SettingsAnimationToggle } from './components/Settings/SettingsAnimationToggle';
import { Header } from './components/Header/Header';
import { Logo } from './components/Logo/Logo';
import { Nav } from './components/Nav/Nav';
import { Footer } from './components/Footer/Footer';
import { Home } from './sections/Home';
import { Portfolio } from './sections/Portfolio';
import { About } from './sections/About';
import { Contact } from './sections/Contact';
import { Resume } from './sections/Resume';
import { Login } from './components/Login/Login';
import { NotFound } from './components/404/NotFound';
import ScrollToTop from './components/ScrollToTop/ScrollToTop';
import './App.scss';

class App extends Component {
  constructor(props) {
    super(props);
    this.sectionsNav = [];
    this.adminUser = false;
    this.state = {
      sections: [
        { name: 'Home', id: 'home', showTitle: false },
        { name: 'Portfolio', id: 'portfolio' },
        { name: 'About', id: 'about' },
        { name: 'Contact', id: 'contact' },
        { name: 'Resume', id: 'resume', linkToPage: true },
      ],
      authenticated: false,
      backendAPI: 'disconnected',
    };
    this.unathenticateUser = this.unathenticateUser.bind(this);
  }

  componentDidMount() {
    this.scrollToTop();
    this.checkUserAuthentication();
    this.buildSectionsNav();
    // this.callBackendAPI()
    //   .then(res => this.setState({ backendAPI: res.express }))
    //   .catch(err =>
    //     console.error(
    //       'Error making backend API call. The backend server is probably disconnected.',
    //       err,
    //     ),
    //   );
  }

  // Only needed when not using netlify's forms

  // callBackendAPI = async () => {
  //   const response = await axios.get('/api/express-backend');
  //   const body = await response.data;

  //   if (response.status !== 200) {
  //     console.error(
  //       `Failed to get a response from the backend API. The backend server is probably disconnected. The following error was returned: "${body.message}"`,
  //     );
  //   }
  //   return body;
  // };

  scrollToTop() {
    window.scrollTo(0, 0);
  }

  checkUserAuthentication() {
    firebaseApp.auth().onAuthStateChanged(user => {
      if (!user) return;
      this.userAuthenticationHandler({ user });
    });
  }

  userAuthenticationHandler = async authData => {
    const admin = firebaseFirestore.collection('admin');
    admin.get().then(querySnapshot => {
      querySnapshot.forEach(doc => {
        const user = doc.data();
        if (authData.user.email === user.user) {
          this.adminUser = true;
          this.sectionsNav.push({
            text: 'Logout',
            onClick: this.unathenticateUser,
          });
          this.setState({ sectionsNav: this.sectionsNav, authenticated: true });
        }
      });
    });
  };

  unathenticateUser() {
    firebaseApp
      .auth()
      .signOut()
      .then(() => {
        this.adminUser = false;
        window.location.href = '/';
      })
      .catch(function(error) {
        console.error('Error logging out: ', error);
      });
  }

  buildSectionsNav() {
    const { sections } = this.state;
    if (sections) {
      sections.map(section => {
        return this.sectionsNav.push({
          text: section.name,
          href: section.linkToPage ? `/${section.id}` : `/#${section.id}`,
        });
      });
      this.setState({ sectionsNav: this.sectionsNav }, () => {
        this.initiateAnchorScroll();
      });
    }
  }

  initiateAnchorScroll() {
    const links = document.querySelectorAll('a[href^="/#"]');
    if (links.length < this.sectionsNav.length) {
      setTimeout(() => {
        return this.initiateAnchorScroll();
      }, 100);
    }
    links.forEach(link => {
      link.addEventListener('click', e => {
        new Promise(resolve => {
          this.scrollToElement(e, e.target, resolve);
        }).catch(err => {
          console.error(`Error while trying to scroll to ${e.target}`, err);
        });
      });
    });
  }

  scrollToElement(e, target, resolve) {
    e.preventDefault();
    let destinationId;
    if (target && target.getAttribute('href')) {
      destinationId = target.getAttribute('href');
    } else if (target.parentNode.getAttribute('href')) {
      destinationId = target.parentNode.getAttribute('href');
    } else {
      destinationId = this.getAttribute('href');
    }
    const distanceToTop = el => Math.floor(el.getBoundingClientRect().top);
    const formattedDestinationId = destinationId
      ? destinationId.replace(/^\//g, '')
      : null;
    const destination = document.querySelector(formattedDestinationId);
    if (!destination) {
      if (!formattedDestinationId) return;
      window.location.href = `/${formattedDestinationId}`;
    } else {
      const startingDistanceFromTop = distanceToTop(destination);
      window.scrollBy({
        top: startingDistanceFromTop - 60,
        left: 0,
        behavior: 'smooth',
      });
    }
    if (resolve) resolve();
  }

  render() {
    const { sectionsNav, sections, portfolioPieces } = this.state;
    return (
      <div className='App'>
        <Header hasImg={true} modifiers={['main', 'has-image']}>
          <a href='/#home' className='logo' tabIndex={1}>
            <Logo />
          </a>
          {sectionsNav && (
            <Nav
              navItems={sectionsNav}
              mobileMenu={true}
              modifiers={['fixed']}
            />
          )}
        </Header>
        <SettingsAnimationToggle />
        <Router>
          <ScrollToTop />
          <Switch>
            <Route exact path='/'>
              {sections && (
                <main>
                  <Home sectionData={sections[0]} />
                  <Portfolio
                    sectionData={sections[1]}
                    portfolio={portfolioPieces}
                    admin={this.adminUser}
                  />
                  <About sectionData={sections[2]} />
                  <Contact sectionData={sections[3]} />
                </main>
              )}
            </Route>
            <Route
              exact
              path='/resume'
              component={() => <Resume sectionData={sections[4]} />}
            />
            <Route
              exact
              path='/admin/login'
              component={() => (
                <Login
                  unathenticate={() => this.unathenticateUser()}
                  authenticated={this.state.authenticated}
                />
              )}
            />
            <Route component={NotFound} />
          </Switch>
        </Router>
        <Footer />
      </div>
    );
  }
}

export default App;
