import React from 'react';
import { Section } from '../Section/Section';
import { CTAButton } from '../CTAButton/CTAButton';

const NotFound = () => (
  <Section
    key='c-section--not-found'
    id='not-found'
    showTitle={false}
    name='404 - Not Found'
    modifiers={['not-found']}
  >
    <div className='c-section__twinkling-stars' data-moving-objects></div>
    <article>
      <p className='c-section__main-intro'>Oh, no!</p>
      <p className='c-section__main-intro'>Somehow we got lost in space.</p>
      <p className='c-section__secondary-intro'>
        Let's get you back <a href='/'>home</a>.
      </p>
      <CTAButton
        text='Travel Home'
        href='/'
        newTab={false}
        modifiers={['large']}
      />
    </article>
    <div className='c-section__landing-image'>
      <img
        src='https://res.cloudinary.com/adamknee/image/upload/v1567473231/portfolio/adam-astronaut-landing-image_y71xr3.svg'
        alt='Astronaut man floating in Space.'
        data-floating-object
      />
    </div>
  </Section>
);

export { NotFound };
