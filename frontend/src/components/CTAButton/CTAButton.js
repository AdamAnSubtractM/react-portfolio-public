import React from 'react';
import PropTypes from 'prop-types';
import { buildClasses } from '../../js/helpers';
import './CTAButton.scss';

const CTAButton = props => {
  const component = 'c-cta-button';
  if (props.href) {
    return (
      <a
        className={`${component} ${buildClasses(component, props.modifiers)}`}
        onClick={props.clickAction ? () => props.clickAction() : null}
        href={props.href ? props.href : `javascript:void(0);`}
        title={`Click to ${props.text}`}
        target={props.newTab ? '_blank' : '_self'}
      >
        {props.text}
      </a>
    );
  } else {
    return (
      <button
        className={`${component} ${buildClasses(component, props.modifiers)}`}
        onClick={props.clickAction ? () => props.clickAction() : null}
        type="submit"
      >
        {props.text}
      </button>
    );
  }
};

CTAButton.propTypes = {
  text: PropTypes.string.isRequired,
  href: PropTypes.string,
  newTab: PropTypes.bool,
  clickAction: PropTypes.func,
  modifiers: PropTypes.array,
};

export { CTAButton };
