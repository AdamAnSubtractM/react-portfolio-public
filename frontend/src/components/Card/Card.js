import React from 'react';
import PropTypes from 'prop-types';
import './Card.scss';

const Card = props => {
  const component = 'c-card';
  return (
    <div
      className={component}
      style={{ backgroundColor: props.bgColor ? props.bgColor : `initial` }}
    >
      {props.children}
    </div>
  );
};

Card.propTypes = {
  bgColor: PropTypes.string,
};

export { Card };
