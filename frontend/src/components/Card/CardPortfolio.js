import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Card } from './Card';
import { CTAButton } from '../CTAButton/CTAButton';
import { buildClasses } from '../../js/helpers';
import './CardPortfolio.scss';

class CardPortfolio extends Component {
  constructor(props) {
    super(props);
    this.CardPortfolioInfo = React.createRef();
  }

  render() {
    const component = 'c-portfolio-card';
    const {
      admin,
      cardImage,
      name,
      summary,
      href,
      openItemModal,
      editItem,
      deleteItem,
    } = this.props;
    return (
      <div
        className={`${component} ${buildClasses(
          component,
          this.props.modifiers,
        )}`}
      >
        <Card>
          <div
            className={`${component}__image`}
            style={{ backgroundImage: `url(${cardImage})` }}
          />
          <div className={`${component}__info`}>
            <h4>{name}</h4>
            <p>{summary}</p>
          </div>
          <div className={`${component}__cta-wrap`}>
            <CTAButton
              key={`${component}--read-more`}
              text='Read More'
              clickAction={() => openItemModal()}
              modifiers={['alternate']}
            />
            <CTAButton
              key={`${component}--launch-project`}
              text='Launch'
              href={href}
              newTab={true}
            />
            {admin && (
              <Fragment>
                <CTAButton
                  key={`${component}--delete-item`}
                  text='Delete'
                  clickAction={() => deleteItem()}
                  modifiers={['delete']}
                />
                <CTAButton
                  key={`${component}--edit-item`}
                  text='Edit'
                  clickAction={() => editItem()}
                  modifiers={['edit']}
                />
              </Fragment>
            )}
          </div>
          {this.props.children}
        </Card>
      </div>
    );
  }
}

CardPortfolio.propTypes = {
  modifiers: PropTypes.array,
  admin: PropTypes.bool.isRequired,
  cardImage: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  summary: PropTypes.string.isRequired,
  href: PropTypes.string.isRequired,
};

export { CardPortfolio };
