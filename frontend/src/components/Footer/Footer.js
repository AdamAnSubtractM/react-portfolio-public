import React from 'react';
import './Footer.scss';

const component = 'c-footer';
const Footer = () => (
  <footer className={component}>
    <p>
      Made with{' '}
      <a href='https://reactjs.org/' target='_blank' rel='noopener noreferrer'>
        ReactJS
      </a>{' '}
      and a lot of{' '}
      <span role='img' aria-label='coffee'>
        ☕
      </span>
      {`${String.fromCharCode(169)} ${new Date().getFullYear()}`} Adam Knee. All
      Rights Reserved.
    </p>
  </footer>
);

export { Footer };
