import React, { Component, Fragment } from 'react';
import axios from 'axios';
import { Loading } from '../Loading/Loading';
import { Checkmark } from '../SVG/Checkmark';
import './FormContact.scss';

const encode = data => {
  return Object.keys(data)
    .map(key => encodeURIComponent(key) + '=' + encodeURIComponent(data[key]))
    .join('&');
};

class FormContactNetlify extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      success: null,
      sendingMail: false,
      formData: {
        name: '',
        email: '',
        message: '',
      },
    };
  }

  handleFormChange(e) {
    const target = e.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    this.setState(prevState => ({
      formData: {
        ...prevState.formData,
        [name]: value,
      },
    }));
  }

  handleFormSubmit(e) {
    e.preventDefault();
    this.handleFormReset();
    new Promise(resolve => {
      this.handleFormReset(resolve);
    })
      .then(() => {
        const formValues = Object.values(this.state.formData);
        formValues.map(value => {
          if (value == null || value === '') {
            return this.handleFormError(
              'Please ensure all required form fields are filled out.',
            );
          }
          return true;
        });
      })
      .then(() => {
        this.isFormValid(Boolean(!this.state.error));
      });
  }

  handleFormReset(resolve) {
    this.setState({ error: false, success: false }, () => {
      if (resolve) {
        return resolve();
      }
    });
  }

  isFormValid(isValid) {
    if (isValid) {
      this.setState({ sendingMail: true }, () => {
        axios({
          method: 'POST',
          headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
          url: '/',
          data: encode({ 'form-name': 'contact', ...this.state.formData }),
        })
          .then(response => {
            if (response.status === 200) {
              this.setState({ sendingMail: false }, () => {
                return this.handleFormSuccess(
                  "Your message is on its way to my inbox. I'll respond to you shortly!",
                );
              });
            } else {
              return this.handleFormError(
                'Your message got lost in space... Please try sending it again!',
              );
            }
          })
          .catch(err => {
            console.error('Error sending email to Adam: ', err);
            return this.handleFormError(
              'Your message got lost in space... Please try sending it again!',
            );
          });
      });
    }
  }

  handleFormSuccess(success) {
    this.setState({
      success,
      error: false,
      loading: false,
      sendingMail: false,
    });
  }

  handleFormError(error) {
    this.setState({
      error,
      success: false,
      loading: false,
      sendingMail: false,
    });
  }

  render() {
    const component = 'c-form-contact';
    const { error, success, sendingMail } = this.state;
    const { name, email, message } = this.state.formData;
    const messageStateModifier = error
      ? `${component}__row--error`
      : `${component}__row--success`;
    return (
      <form className={component} onSubmit={e => this.handleFormSubmit(e)}>
        {(error || success) && (
          <div className={`${component}__row ${messageStateModifier}`}>
            {error && <p>{error}</p>}
            {success && <p>{success}</p>}
          </div>
        )}
        {sendingMail && (
          <div className={`${component}__row ${component}__row--sending`}>
            <div className={`${component}__loading`}>
              <p>Attempting to send your message...</p>{' '}
              {<Loading color='#fff' />}
            </div>
          </div>
        )}
        {success && <Checkmark color='#fff' />}
        {!success && (
          <Fragment>
            <div className={`${component}__row`}>
              <label>Name:</label>
              <input
                name='name'
                type='text'
                autoFocus
                placeholder={'Kevin Hart'}
                defaultValue={name}
                onChange={e => this.handleFormChange(e)}
                required
              />
            </div>
            <div className={`${component}__row`}>
              <label>Email:</label>
              <input
                name='email'
                type='email'
                autoFocus
                placeholder={'kevinhart@gmail.com'}
                defaultValue={email}
                onChange={e => this.handleFormChange(e)}
                required
              />
            </div>
            <div className={`${component}__row`}>
              <label>Message:</label>
              <textarea
                name='message'
                autoFocus
                defaultValue={message}
                placeholder={"Hey Adam, what's up?"}
                onChange={e => this.handleFormChange(e)}
                required
              />
            </div>
            <div className={`${component}__row`}>
              <input
                autoFocus
                className={`${component}__submit-button`}
                type='submit'
                value='Send Message'
                tabIndex={0}
              />
            </div>
          </Fragment>
        )}
      </form>
    );
  }
}

export { FormContactNetlify };
