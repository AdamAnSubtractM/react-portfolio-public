import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { buildClasses } from '../../js/helpers';
import './Header.scss';

class Header extends Component {
  state = {
    modifiers: null,
    hasImg: null,
  };

  componentDidMount() {
    this.setState({
      modifiers: this.props.modifiers,
      hasImg: this.props.hasImg,
    });
  }

  render() {
    const component = 'c-header';
    return (
      <header
        className={`${component} ${buildClasses(
          component,
          this.props.modifiers,
        )}`}
      >
        {this.props.children}
      </header>
    );
  }
}

Header.defaultProps = {
  hasImg: false,
};

Header.propTypes = {
  modifiers: PropTypes.array,
  hasImg: PropTypes.bool.isRequired,
};

export { Header };
