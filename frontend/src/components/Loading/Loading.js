import React from 'react';
import './Loading.scss';

const Loading = ({ color }) => (
  <div className='c-ellipsis'>
    <div style={{ backgroundColor: color }}></div>
    <div style={{ backgroundColor: color }}></div>
    <div style={{ backgroundColor: color }}></div>
    <div style={{ backgroundColor: color }}></div>
  </div>
);

export { Loading };
