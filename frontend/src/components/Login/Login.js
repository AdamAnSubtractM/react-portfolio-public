import React, { Component } from 'react';
import { firebaseApp, firebaseAuthProvider } from '../../js/firebase';
import { Section } from '../Section/Section';
import { CTAButton } from '../CTAButton/CTAButton';
import './Login.scss';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      authenticated: false,
    };
    this.authenticate = this.authenticate.bind(this);
  }

  componentDidMount() {
    if (this.props.authenticated) {
      this.setState({ authenticated: true });
    }
  }

  authenticate(provider) {
    if (this.state.authenticated) {
      this.props.unathenticate();
    } else {
      firebaseApp
        .auth()
        .signInWithPopup(firebaseAuthProvider(provider))
        .then(() => {
          this.setState({ authenticated: true });
        })
        .then(() => {
          window.location.href = '/';
        })
        .catch(error => {
          console.error('Authentication Error:', error);
        });
    }
  }

  render() {
    const component = 'c-login';
    return (
      <Section
        key='c-section--login'
        id='login'
        showTitle={false}
        name='Login'
        modifiers={['login']}
      >
        <div className='c-section__twinkling-stars' data-moving-objects></div>
        <div className={component} onClick={() => this.authenticate('Github')}>
          <CTAButton
            text={this.state.authenticated ? 'Log Out' : 'Log In'}
            modifiers={['large']}
          />
        </div>
      </Section>
    );
  }
}

export { Login };
