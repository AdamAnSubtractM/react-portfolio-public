import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { buildClasses } from '../../js/helpers';
import './Modal.scss';

class Modal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modifiers: null,
      opened: null,
      title: null,
    };
    this.Modal = React.createRef();
  }

  componentDidMount() {
    this.setState(
      {
        modifiers: this.props.modifiers,
        title: this.props.title,
      },
      () => {
        this.setBodyState();
      },
    );
  }

  componentDidUpdate() {
    if (this.props.title !== this.state.title) {
      this.setState({ title: this.props.title });
    }
  }

  setBodyState() {
    const html = document.querySelector('html');
    if (this.state.opened) {
      html.classList.add('html', 'html--modal-opened');
    } else {
      html.classList.remove('html', 'html--modal-opened');
    }
  }

  closeModal() {
    this.setState({ opened: false }, () => {
      this.setBodyState();
      document.removeEventListener('keydown', this.escapeModal, false);
    });
  }

  openModal = () => {
    this.setState({ opened: true }, () => {
      const modalClose = this.Modal.current.querySelector('.c-modal__close');
      modalClose.focus();
      this.setBodyState();
      document.addEventListener('keydown', this.escapeModal, false);
    });
  };

  escapeModal = e => {
    if (e.key === 'Escape') {
      this.closeModal();
    }
    if (e.key === 'Tab') {
      const modalLaunch = this.Modal.current.querySelector('.c-cta-button');
      const modalClose = this.Modal.current.querySelector('.c-modal__close');
      setTimeout(() => {
        const activeFocus = document.activeElement;
        const body = document.querySelector('body');
        if (
          activeFocus !== modalClose &&
          activeFocus !== modalLaunch &&
          activeFocus !== body
        ) {
          this.closeModal();
        }
      }, 100);
    }
  };

  render() {
    const component = 'c-modal';
    const { opened, title } = this.state;
    if (opened) {
      return (
        <Fragment>
          <div
            ref={this.Modal}
            className={`${component} ${buildClasses(
              component,
              this.props.modifiers,
            )}`}
          >
            <div className={`${component}__title`}>
              {title && <h4>{title}</h4>}
              <button
                className={`${component}__close`}
                onClick={() => this.closeModal()}
              />
            </div>
            <div className={`${component}__content`}>{this.props.children}</div>
          </div>
          <div
            className={`${component}__backdrop`}
            onClick={() => this.closeModal()}
            tabIndex={1}
          />
        </Fragment>
      );
    } else {
      return null;
    }
  }
}

Modal.propTypes = {
  modifiers: PropTypes.array,
  title: PropTypes.string,
};

export { Modal };
