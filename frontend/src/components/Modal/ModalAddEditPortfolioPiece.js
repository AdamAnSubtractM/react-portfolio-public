import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { firebaseFirestore } from '../../js/firebase';
import { buildClasses } from '../../js/helpers';
import { Modal } from './Modal';
import { CTAButton } from '../CTAButton/CTAButton';
import './ModalAddEditPortfolioPiece.scss';

class ModalAddEditPortfolioPiece extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modifiers: null,
      currentPortfolioID: 0,
      formAction: 'Add',
      formData: {
        id: '',
        orderID: null,
        name: '',
        summary: '',
        description: '',
        cardImage: '',
        largeImage: '',
        link: '',
        visible: true,
      },
    };
    this.firebaseID = null;
    this.Modal = React.createRef();
    this.addItem = this.addItem.bind(this);
  }

  componentDidMount() {
    const { modifiers, currentPortfolioID } = this.props;
    this.setState({
      modifiers: modifiers,
      currentPortfolioID,
    });
  }

  setActiveItem = (item, resolveAction) => {
    new Promise(resolve => {
      this.handleFormReset(resolve);
    })
      .then(() => {
        this.firebaseID = item.firebaseID;
        this.setState(prevState => ({
          formData: {
            ...prevState.formData,
            id: item.firebaseId,
            orderID: item.orderID,
            name: item.name,
            summary: item.summary,
            description: item.description,
            cardImage: item.cardImage,
            largeImage: item.largeImage,
            link: item.link,
            visible: item.visible,
          },
        }));
      })
      .then(() => {
        resolveAction();
      });
  };

  addItem = () => {
    new Promise(resolve => {
      this.handleFormReset(resolve);
    }).then(() => {
      this.setState(
        {
          formAction: 'Add',
          currentPortfolioID: this.state.currentPortfolioID + 1,
        },
        () => {
          this.Modal.current.openModal();
        },
      );
    });
  };

  editItem = item => {
    new Promise(resolveAction => {
      this.setActiveItem(item, resolveAction);
    }).then(() => {
      this.setState(
        {
          formAction: 'Edit',
        },
        () => {
          this.Modal.current.openModal();
        },
      );
    });
  };

  deleteItem = item => {
    firebaseFirestore
      .collection('portfolio')
      .doc(item.id)
      .delete()
      .then(() => {
        console.log(`${item.name} was successfully deleted.`);
        this.props.fetchPortfolio();
      })
      .catch(error => {
        console.error('Error deleting item: ', error);
      });
  };

  handleFormChange(e) {
    const target = e.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    this.setState(prevState => ({
      formData: {
        ...prevState.formData,
        [name]: value,
      },
    }));
  }

  handleFormSubmit(e) {
    e.preventDefault();
    const postData = this.state.formData;
    postData.orderID = this.state.formData.orderID
      ? this.state.formData.orderID
      : this.state.currentPortfolioID;
    postData.id = `${postData.orderID}`;
    firebaseFirestore
      .collection('portfolio')
      .doc(postData.id)
      .set(postData)
      .then(() => {
        new Promise(resolve => {
          this.handleFormReset(resolve);
          this.props.fetchPortfolio();
        }).then(() => {
          this.Modal.current.closeModal();
        });
      })
      .catch(error => {
        console.error('Error saving portfolio item: ', error);
      });
  }

  handleFormReset(resolve) {
    this.setState(
      prevState => ({
        formData: {
          ...prevState.formData,
          id: '',
          orderID: null,
          name: '',
          summary: '',
          description: '',
          cardImage: '',
          largeImage: '',
          link: '',
          visible: true,
        },
      }),
      () => {
        resolve();
      },
    );
  }

  render() {
    const component = 'c-modal-add-edit-portfolio-piece';
    const {
      name,
      summary,
      description,
      cardImage,
      largeImage,
      link,
      visible,
    } = this.state.formData;
    return (
      <Fragment>
        <Modal
          ref={this.Modal}
          title={`${this.state.formAction} Portfolio Item`}
          key={this.props.keyValue}
        >
          <form
            className={`${component} ${buildClasses(
              component,
              this.props.modifiers,
            )}`}
            onSubmit={e => this.handleFormSubmit(e)}
          >
            <div className={`${component}__row`}>
              <label>Name:</label>
              <input
                name='name'
                type='text'
                autoFocus
                defaultValue={name}
                required
                onChange={e => this.handleFormChange(e)}
              />
            </div>
            <div className={`${component}__row`}>
              <label>Summary:</label>
              <input
                name='summary'
                type='text'
                defaultValue={summary}
                required
                onChange={e => this.handleFormChange(e)}
              />
            </div>
            <div className={`${component}__row`}>
              <label>Description:</label>
              <textarea
                name='description'
                type='text'
                defaultValue={description}
                required
                onChange={e => this.handleFormChange(e)}
              />
            </div>
            <div className={`${component}__row`}>
              <div className={`${component}__column`}>
                <label>Card Image:</label>
                <input
                  name='cardImage'
                  type='text'
                  defaultValue={cardImage}
                  required
                  onChange={e => this.handleFormChange(e)}
                />
              </div>
              <div className={`${component}__column`}>
                <label>Large Image:</label>
                <input
                  name='largeImage'
                  type='text'
                  defaultValue={largeImage}
                  required
                  onChange={e => this.handleFormChange(e)}
                />
              </div>
            </div>
            <div className={`${component}__row`}>
              <div className={`${component}__column`}>
                <label>Link:</label>
                <input
                  name='link'
                  type='text'
                  defaultValue={link}
                  required
                  onChange={e => this.handleFormChange(e)}
                />
              </div>
              <div className={`${component}__column`}>
                <label>Visible:</label>
                <select
                  name='visible'
                  defaultValue={visible}
                  onChange={e => this.handleFormChange(e)}
                >
                  <option value={true}>True</option>
                  <option value={false}>False</option>
                </select>
              </div>
            </div>
            <div className={`${component}__row ${component}__row--flex-end`}>
              <CTAButton text={`Submit`} />
            </div>
          </form>
        </Modal>
        <CTAButton
          clickAction={() => this.addItem()}
          text={`Add Portfolio Item`}
        />
      </Fragment>
    );
  }
}

ModalAddEditPortfolioPiece.propTypes = {
  modifiers: PropTypes.array,
  fetchPortfolio: PropTypes.func.isRequired,
  currentPortfolioID: PropTypes.number.isRequired,
  keyValue: PropTypes.any.isRequired,
};

export { ModalAddEditPortfolioPiece };
