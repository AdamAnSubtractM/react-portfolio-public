import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Modal } from './Modal';
import { CTAButton } from '../CTAButton/CTAButton';
import './ModalPortfolioItem.scss';

class ModalPortfolioItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      largeImage: null,
      name: null,
      description: null,
      href: null,
    };
    this.Modal = React.createRef();
  }

  openItemModal = card => {
    const { largeImage, name, description, link } = card;
    this.setState({ largeImage, name, description, href: link }, () => {
      this.Modal.current.openModal();
    });
  };

  render() {
    const component = 'c-modal-portfolio-item';
    const { largeImage, name, description, href } = this.state;
    return (
      <Modal
        key={this.props.keyValue}
        ref={this.Modal}
        title='More Deatils About The Project'
      >
        <div className={component}>
          <div className={`${component}__feature-image`}>
            <img alt='test' src={largeImage} />
          </div>
          <article className={`${component}__item-details`}>
            <h3>{name}</h3>
            <p>{description}</p>
            <div className={`${component}__cta-wrap`}>
              <CTAButton text='Launch' href={href} newTab={true} tabIndex={1} />
            </div>
          </article>
        </div>
      </Modal>
    );
  }
}

ModalPortfolioItem.propTypes = {
  keyValue: PropTypes.any.isRequired,
};

export { ModalPortfolioItem };
