import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { buildClasses } from '../../js/helpers';
import { throttle } from 'lodash';
import './Nav.scss';

class Nav extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modifiers: null,
      navItems: null,
      mobileMenu: null,
      mobileMenuOpen: null,
    };
  }

  componentDidMount() {
    const { navItems, mobileMenu } = this.props;
    this.setState(
      {
        navItems,
        mobileMenu,
      },
      () => {
        this.registerOnScrollResizeEvent(this.state.mobileMenu);
      },
    );
  }

  registerOnScrollResizeEvent(shouldRegister) {
    if (shouldRegister) {
      window.addEventListener(
        'resize',
        throttle(() => {
          const viewport = window.innerWidth;
          if (viewport >= 640 && this.state.mobileMenuOpen) {
            this.closeMobileMenu();
          }
        }),
        500,
      );
    }
  }

  toggleMobileMenu() {
    this.toggleMobileClasses();
    this.setState({ mobileMenuOpen: !this.state.mobileMenuOpen }, () => {
      if (this.state.mobileMenuOpen) {
        document.addEventListener('keydown', this.listenForEscape, false);
      } else {
        document.removeEventListener('keydown', this.listenForEscape, false);
      }
    });
  }

  closeMobileMenu(navItem) {
    this.toggleMobileClasses();
    if (this.state.mobileMenu) {
      this.setState({ mobileMenuOpen: false }, () => {
        document.removeEventListener('keydown', this.listenForEscape, false);
      });
    }
    if (navItem && navItem.onClick) {
      navItem.onClick();
    }
  }

  listenForEscape = e => {
    if (e.key === 'Escape') {
      this.closeMobileMenu();
    }
  };

  toggleMobileClasses() {
    const viewport = window.innerWidth;
    const html = document.querySelector('html');
    if (viewport >= 640) return;
    if (html.classList.contains('html--mobile-nav-opened')) {
      html.classList.remove('html', 'html--mobile-nav-opened');
    } else {
      html.classList.add('html', 'html--mobile-nav-opened');
    }
  }

  render() {
    const { navItems, mobileMenuOpen, mobileMenu } = this.state;
    const { modifiers } = this.props;
    if (navItems) {
      const component = 'c-nav';
      const openedModifier = mobileMenuOpen ? `${component}--opened` : '';
      return (
        <nav
          className={`${component} ${buildClasses(
            component,
            modifiers,
          )} ${openedModifier}`}
        >
          {mobileMenu && (
            <button
              className={`${component}__menu`}
              onClick={() => this.toggleMobileMenu()}
            >
              Menu
            </button>
          )}
          <ul role='navigation'>
            {navItems.map((navItem, key) => {
              if (navItem.href) {
                return (
                  <li
                    key={key}
                    onClick={() => {
                      this.closeMobileMenu(navItem);
                    }}
                  >
                    <a href={`${navItem.href}`}>{navItem.text}</a>
                  </li>
                );
              } else {
                if (navItem.onClick) {
                  return (
                    <li
                      key={key}
                      onClick={() => {
                        this.closeMobileMenu(navItem);
                      }}
                    >
                      <button>{navItem.text}</button>
                    </li>
                  );
                } else {
                  return (
                    <li
                      key={key}
                      onClick={() => {
                        this.closeMobileMenu(navItem);
                      }}
                    >
                      {navItem.text}
                    </li>
                  );
                }
              }
            })}
          </ul>
          {mobileMenu && <div className={`${component}__splash`}></div>}
        </nav>
      );
    } else {
      return null;
    }
  }
}

Nav.propTypes = {
  modifiers: PropTypes.array,
  mobileMenu: PropTypes.bool,
  navItems: PropTypes.arrayOf(
    PropTypes.shape({
      text: PropTypes.string.isRequired,
      href: PropTypes.string,
      onClick: PropTypes.func,
    }),
  ).isRequired,
};

export { Nav };
