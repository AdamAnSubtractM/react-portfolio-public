import React from 'react';
import './ScrollIcon.scss';

const ScrollIcon = () => (
  <div className='c-scroll-icon' data-floating-object>
    <a className='c-scroll-icon__container' href='/#portfolio'>
      <svg
        xmlns='http://www.w3.org/2000/svg'
        viewBox='0 0 50 50'
        width='50px'
        fill='#f2f2f2'
      >
        <path d='M48.707 19.353l-1.414-1.413L25 40.232 2.707 17.94l-1.414 1.413L25 43.06z' />
        <path d='M48.707 8.353L47.293 6.94 25 29.232 2.707 6.94 1.293 8.353 25 32.06z' />
      </svg>
    </a>
  </div>
);

export { ScrollIcon };
