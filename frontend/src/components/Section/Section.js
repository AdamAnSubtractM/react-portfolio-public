import React from 'react';
import PropTypes from 'prop-types';
import { buildClasses } from '../../js/helpers';
import './Sections.scss';

const Section = props => {
  const component = 'c-section';
  if (props.visible) {
    return (
      <section
        id={props.id}
        className={`${component} ${buildClasses(component, props.modifiers)}`}
      >
        <div className='c-section__container'>
          {props.showTitle && props.name && (
            <h2 className='c-section__heading'>{props.name}</h2>
          )}
          <div className='c-section__children'>{props.children}</div>
        </div>
      </section>
    );
  } else {
    return null;
  }
};

Section.defaultProps = {
  showTitle: true,
  visible: true,
};

Section.propTypes = {
  modifiers: PropTypes.array,
  showTitle: PropTypes.bool,
  name: PropTypes.string,
  visible: PropTypes.bool,
  id: PropTypes.string.isRequired,
  children: PropTypes.any.isRequired,
};

export { Section };
