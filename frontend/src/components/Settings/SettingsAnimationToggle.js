import React, { Component } from 'react';
import { SettingsCog } from '../SVG/SettingsCog';
import { SettingsToggleSwitch } from '../Settings/SettingsToggleSwitch';
import './SettingsAnimationToggle.scss';

class SettingsAnimationToggle extends Component {
  constructor(props) {
    super(props);
    this.state = {
      settingsPanelOpen: false,
      animationsEnabled: true,
    };
  }

  componentDidMount() {
    document.addEventListener('keydown', this.escapePanel, false);
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.escapePanel, false);
  }

  toggleSettingsPanel() {
    this.setState({ settingsPanelOpen: !this.state.settingsPanelOpen });
  }

  toggleAnimationsSwitch() {
    this.setState({ animationsEnabled: !this.state.animationsEnabled }, () => {
      const body = document.querySelector('.App');
      if (!this.state.animationsEnabled) {
        body.classList.add('animations-disabled');
      } else {
        body.classList.remove('animations-disabled');
      }
    });
  }

  escapePanel = (e) => {
    if (e.key === 'Escape' && this.state.settingsPanelOpen) {
      this.toggleSettingsPanel();
    }
  } 

  render() {
    return (
      <div className='c-settings-cog'>
        <button
          className='c-settings-cog__button'
          onClick={() => this.toggleSettingsPanel()}
        >
          <SettingsCog />
        </button>
        {this.state.settingsPanelOpen && (
          <div className='c-settings-cog__control-panel'>
            <button className='c-settings-cog__close' onClick={() => this.toggleSettingsPanel()}></button>
            <p>
              Animations {this.state.animationsEnabled ? `Enabled` : `Disabled`}
              :
            </p>
            <span onClick={() => this.toggleAnimationsSwitch()}>
              <SettingsToggleSwitch checked={this.state.animationsEnabled} />
            </span>
          </div>
        )}
      </div>
    );
  }
}

export { SettingsAnimationToggle };
