import React from 'react';
import './SettingsToggleSwitch.scss';

const SettingsToggleSwitch = ({ checked }) => {
  return (
    <button className='c-settings-toggle-switch'>
      <input
        type='checkbox'
        className='c-settings-toggle-switch__input'
        checked={checked ? 'checked' : false}
        readOnly
      />
      <label htmlFor='switch' className='c-settings-toggle-switch__label'>
        Toggle On and Off
      </label>
    </button>
  );
};

export { SettingsToggleSwitch };
