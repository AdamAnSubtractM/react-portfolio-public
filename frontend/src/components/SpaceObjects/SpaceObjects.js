import React from 'react';
import { AsteroidRound } from '../SVG/AsteroidRound';
import { AsteroidOrganic } from '../SVG/AsteroidOrganic';
import { AsteroidRigid } from '../SVG/AsteroidRigid';
import { Satellite } from '../SVG/Satellite';

const SpaceObjects = () => {
  return (
    <div data-moving-objects>
      <div
        className='c-section__floating-space-object c-section__floating-space-object--satellite'
        style={{
          zIndex: `100`,
          top: `-5%`,
          animation: `floatingAcross 80s linear infinite`,
        }}
      >
        <Satellite width={75} />
      </div>
      <div
        className='c-section__floating-asteroid c-section__floating-asteroid--round'
        style={{
          zIndex: `100`,
          top: `10%`,
          animation: `floatingAcross 36s linear infinite`,
        }}
      >
        <AsteroidRound width={100} animation={`spinning 10s linear infinite`} />
      </div>
      <div
        className='c-section__floating-asteroid c-section__floating-asteroid--organic'
        style={{
          zIndex: `100`,
          top: `34%`,
          animation: `floatingAcross 62s linear infinite`,
        }}
      >
        <AsteroidOrganic width={50} animation={`spinning 8s linear infinite`} />
      </div>
      <div
        className='c-section__floating-asteroid c-section__floating-asteroid--rigid'
        style={{
          zIndex: `100`,
          top: `70%`,
          animation: `floatingAcross 32s linear infinite`,
        }}
      >
        <AsteroidRigid width={65} animation={`spinning 18s linear infinite`} />
      </div>
      <div
        className='c-section__floating-asteroid c-section__floating-asteroid--organic'
        style={{
          zIndex: `100`,
          top: `48%`,
          animation: `floatingAcross 20s linear infinite`,
        }}
      >
        <AsteroidOrganic
          width={90}
          animation={`spinning 15s linear infinite`}
        />
      </div>
      <div
        className='c-section__floating-asteroid c-section__floating-asteroid--rigid'
        style={{
          zIndex: `100`,
          top: `64%`,
          animation: `floatingAcross 100s linear infinite`,
        }}
      >
        <AsteroidRigid width={120} animation={`spinning 25s linear infinite`} />
      </div>
      <div
        className='c-section__floating-asteroid c-section__floating-asteroid--rigid'
        style={{
          zIndex: `100`,
          top: `18%`,
          animation: `floatingAcross 42s linear infinite`,
        }}
      >
        <AsteroidRigid width={90} animation={`spinning 25s linear infinite`} />
      </div>
      <div
        className='c-section__floating-asteroid c-section__floating-asteroid--organic'
        style={{
          zIndex: `100`,
          top: `80%`,
          animation: `floatingAcross 50s linear infinite`,
        }}
      >
        <AsteroidOrganic width={90} animation={`spinning 8s linear infinite`} />
      </div>
    </div>
  );
};

export { SpaceObjects };
