import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

const firebaseApp = firebase.initializeApp({
  apiKey: 'R@ndomApiK3yGO3$Her3',
  authDomain: 'your-domain.firebaseapp.com',
  databaseURL: 'https://your-domain.firebaseio.com',
  projectId: 'project-id-here',
});

const firebaseFirestore = firebase.firestore();

const firebaseAuthProvider = provider =>
  new firebase.auth[`${provider}AuthProvider`]();

export { firebaseApp, firebaseFirestore, firebaseAuthProvider };
