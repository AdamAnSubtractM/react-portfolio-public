import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

const firebaseApp = firebase.initializeApp({
  apiKey: 'AIzaSyDPIOC_gJfAGV6rpVyMqVflrDSKB_G6Stg',
  authDomain: 'portfolio-pieces.firebaseapp.com',
  databaseURL: 'https://portfolio-pieces.firebaseio.com',
  projectId: 'portfolio-pieces',
});

const firebaseFirestore = firebase.firestore();

const firebaseAuthProvider = provider =>
  new firebase.auth[`${provider}AuthProvider`]();

export { firebaseApp, firebaseFirestore, firebaseAuthProvider };
