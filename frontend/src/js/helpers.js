export function buildClasses(component, modifiers) {
  if (modifiers) {
    let classes = [];
    modifiers.map(modifier => {
      return classes.push(`${component}--${modifier}`);
    });
    return classes.join(' ');
  }
  return '';
}

export function isActivelyViewed(elem) {
  let boundaries = elem.getBoundingClientRect();
  return boundaries.top <= 0 && boundaries.bottom >= 0;
}
