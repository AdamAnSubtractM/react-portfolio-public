import React from 'react';
import PropTypes from 'prop-types';
import { Section } from '../components/Section/Section';
import { Book } from '../components/SVG/Book';
import { Dumbbell } from '../components/SVG/Dumbbell';
import { Volleyball } from '../components/SVG/Volleyball';

const About = props => (
  <Section
    key={`c-section--${props.sectionData.id}`}
    id={props.sectionData.id}
    name={props.sectionData.name}
    showTitle={props.sectionData.showTitle}
    modifiers={[props.sectionData.id]}
    navData={props.navData}
  >
    <article className='c-section__about-me-copy'>
      <section className='c-section__body-copy c-section__body-copy--my-story'>
        <h3 className='c-section__sub-heading'>My Story</h3>
        <p>
          Hey, so I'm Adam Knee and yeah... that's actually my last name. I like
          to consider myself a Frontend Ninja. I have many skills revolving
          around technology and I love to keep honing those skills to become a
          better engineer. I currently am focusing on my skills in ReactJS at{' '}
          <a href='//wesco.com' target='_blank' rel='noopener noreferrer'>
            WESCO
          </a>
          , previously at{' '}
          <a href='//dicks.com' target='_blank' rel='noopener noreferrer'>
            DICK's Sporting Goods
          </a>
          , but love working in a variety of different tech stacks. Grab a copy
          of my <a href='/resume'>resume</a> for a full list of skills and
          experience.
        </p>
        <p>
          Currently based in Pittsburgh, I attended{' '}
          <a href='//ptcollege.edu'>Pittsburgh Technical Institute</a> and have
          a degree in Multimedia Technologies with a concentration in Responsive
          web design and development. Feel free to{' '}
          <a href='/#contact'>contact me</a> below. I'd love to chat and get to
          know you!
        </p>
      </section>
      <section className='c-section__body-copy c-section__body-copy--interesting-stuff'>
        <h3 className='c-section__sub-heading'>The Interesting Stuff</h3>
        <p>
          I love to lift heavy things. Exercising is by far my favorite hobby.
          Chances are if i'm not coding, working, or learning a new coding
          language, I'm at the gym throwing up some iron. Feel free to follow my
          fitness instagram{' '}
          <a
            href='//www.instagram.com/the_last_barbender/'
            target='_blank'
            rel='noopener noreferrer'
          >
            @the_last_barbender
          </a>{' '}
          if you're into that, but be warned there may be some shirtless
          pictures.
        </p>
        <p>
          The outdoors calm my mind. Nature is my happy place. With that said,
          my other interests include hiking, playing sports, biking, and getting
          some fresh air. During the spring and summer time you'll catch me
          playing on a lot of intramural sports teams. Last but not least, I
          also love to read! My favorite book series is the Kingkiller Chronicle
          and if you're looking for a long and entertaining read, I highly
          suggest that book series! Anything else about me, I'd love to chat
          with you about it over the phone or <a href='#contact'>email</a>. Talk
          to you soon!
        </p>
      </section>
    </article>
    <div className='c-section__landing-image'>
      <div
        className='c-section__floating-object c-section__floating-object--dumbbell'
        data-floating-object
      >
        <Dumbbell />
      </div>
      <div
        className='c-section__floating-object c-section__floating-object--book'
        data-floating-object
      >
        <Book />
      </div>
      <div
        className='c-section__floating-object c-section__floating-object--dumbbell'
        data-floating-object
      >
        <Dumbbell />
      </div>
      <div
        className='c-section__floating-object c-section__floating-object--volleyball'
        data-floating-object
      >
        <Volleyball />
      </div>
      <img
        src='https://res.cloudinary.com/adamknee/image/upload/v1574745983/portfolio/avatar-guy_lz2szh.svg'
        alt='Adam Knee sitting in front of his laptop.'
      />
    </div>
  </Section>
);

About.propTypes = {
  sectionData: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    showTitle: PropTypes.bool,
    modifiers: PropTypes.array,
    navData: PropTypes.array,
  }),
};

export { About };
