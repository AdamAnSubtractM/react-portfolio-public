import React from 'react';
import PropTypes from 'prop-types';
import { Section } from '../components/Section/Section';
import { FormContactNetlify } from '../components/Form/FormContactNetlify';

const Contact = props => (
  <Section
    key={`c-section--${props.sectionData.id}`}
    id={props.sectionData.id}
    name={props.sectionData.name}
    showTitle={props.sectionData.showTitle}
    modifiers={[props.sectionData.id]}
    navData={props.navData}
  >
    <div className='c-section__landing-image'>
      <img
        src='https://res.cloudinary.com/adamknee/image/upload/v1567473231/portfolio/adam-astronaut-contact_wj1axr.svg'
        alt='Astronaut man who is floating in Space, saying goodbye!'
        data-floating-object
      />
    </div>
    <div className='c-section__contact-form'>
      <div className='c-section__contact-form-wrap'>
        <FormContactNetlify />
      </div>
    </div>
  </Section>
);

Contact.propTypes = {
  sectionData: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    showTitle: PropTypes.bool,
    modifiers: PropTypes.array,
    navData: PropTypes.array,
  }),
};

export { Contact };
