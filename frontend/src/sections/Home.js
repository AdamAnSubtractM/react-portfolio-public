import React from 'react';
import PropTypes from 'prop-types';
import { Section } from '../components/Section/Section';
import { ScrollIcon } from '../components/ScrollIcon/ScrollIcon';

const Home = props => (
  <Section
    key={`c-section--${props.sectionData.id}`}
    id={props.sectionData.id}
    name={props.sectionData.name}
    showTitle={props.sectionData.showTitle}
    modifiers={[props.sectionData.id]}
    navData={props.navData}
  >
    <div className='c-section__twinkling-stars' data-moving-objects></div>
    <article>
      <h1 className='sr-only'>
        Adam Knee - Pittsburgh Web Developer and Web Designer
      </h1>
      <h2 className='sr-only'>Adam Knee's Portfolio</h2>
      <p className='c-section__main-intro'>Hello, there.</p>
      <p className='c-section__main-intro'>I'm Adam Knee.</p>
      <p className='c-section__secondary-intro'>
        I build <a href='/#portfolio'>cool stuff</a> that is out of this world.
      </p>
    </article>
    <div className='c-section__landing-image'>
      <img
        src='https://res.cloudinary.com/adamknee/image/upload/v1574746533/portfolio/adam-astronaut-landing-image_qffeut.svg'
        alt='Astronaut man floating in Space.'
        data-floating-object
      />
    </div>
    <ScrollIcon />
  </Section>
);

Home.propTypes = {
  sectionData: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    showTitle: PropTypes.bool,
    modifiers: PropTypes.array,
    navData: PropTypes.array,
  }),
};

export { Home };
