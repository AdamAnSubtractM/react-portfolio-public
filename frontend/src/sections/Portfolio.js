import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { firebaseFirestore } from '../js/firebase';
import { Section } from '../components/Section/Section';
import { CardPortfolio } from '../components/Card/CardPortfolio';
import { ModalPortfolioItem } from '../components/Modal/ModalPortfolioItem';
import { ModalAddEditPortfolioPiece } from '../components/Modal/ModalAddEditPortfolioPiece';
import { SpaceObjects } from '../components/SpaceObjects/SpaceObjects';

class Portfolio extends Component {
  constructor(props) {
    super(props);
    this.state = {
      portfolioPieces: null,
      currentPortfolioID: 0,
    };
    this.ModalPortfolioItem = React.createRef();
    this.ModalAddEditPortfolioPiece = React.createRef();
  }

  componentDidMount() {
    this.fetchPortfolio();
  }

  fetchPortfolio = () => {
    const collectionRef = firebaseFirestore.collection('portfolio');
    let portfolioPieces = [];
    collectionRef
      .orderBy('orderID')
      .get()
      .then(querySnapshot => {
        querySnapshot.forEach(doc => {
          let piece = doc.data();
          piece.firebaseId = doc.id;
          if (piece.visible && piece.visible !== 'false') {
            portfolioPieces.push(piece);
          }
        });
      })
      .then(() => {
        this.setState({
          portfolioPieces,
          currentPortfolioID: portfolioPieces.length,
        });
      })
      .catch(error => {
        console.error("Error fetching Adam's Portfolio: ", error);
      });
  };

  render() {
    const { id, name, showTitle } = this.props.sectionData;
    return (
      <Section
        key={`c-section--${id}`}
        id={id}
        name={name}
        showTitle={showTitle}
        modifiers={[id]}
        navData={this.props.navData}
      >
        <SpaceObjects />
        <div className={`c-section__portfolio-wrap`}>
          {this.state.portfolioPieces &&
            this.state.portfolioPieces.map((card, key) => {
              return (
                <CardPortfolio
                  admin={this.props.admin}
                  editItem={() =>
                    this.ModalAddEditPortfolioPiece.current.editItem(card)
                  }
                  deleteItem={() =>
                    this.ModalAddEditPortfolioPiece.current.deleteItem(card)
                  }
                  openItemModal={() =>
                    this.ModalPortfolioItem.current.openItemModal(card)
                  }
                  modifiers={this.props.admin ? ['admin'] : null}
                  key={`c-portfolio-card--${card.firebaseId}`}
                  id={card.firebaseId}
                  orderID={card.orderID}
                  cardImage={card.cardImage}
                  name={card.name}
                  summary={card.summary}
                  description={card.description}
                  href={card.link}
                >
                  <img
                    alt=''
                    key={`c-portfolio-card__img--${card.firebaseId}`}
                    src={card.largeImage}
                    style={{ display: 'none' }}
                  />
                </CardPortfolio>
              );
            })}
          {this.state.portfolioPieces && (
            <ModalPortfolioItem
              key={`c-modal-portfolio-item--${id}`}
              ref={this.ModalPortfolioItem}
              keyValue={id}
            />
          )}
          {this.props.admin && (
            <div className='c-section__admin-controls c-section__admin-controls--add-item'>
              <ModalAddEditPortfolioPiece
                key={`c-modal-add-edd-portfolio-piece--${id}`}
                ref={this.ModalAddEditPortfolioPiece}
                keyValue={id}
                fetchPortfolio={this.fetchPortfolio}
                currentPortfolioID={this.state.currentPortfolioID}
              />
            </div>
          )}
        </div>
      </Section>
    );
  }
}

Portfolio.propTypes = {
  sectionData: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    showTitle: PropTypes.bool,
    modifiers: PropTypes.array,
    navData: PropTypes.array,
  }),
};

export { Portfolio };
