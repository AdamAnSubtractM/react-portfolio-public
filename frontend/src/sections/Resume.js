import React from 'react';
import PropTypes from 'prop-types';
import { Section } from '../components/Section/Section';
import { CTAButton } from '../components/CTAButton/CTAButton';

const Resume = props => (
  <Section
    key={`c-section--${props.sectionData.id}`}
    id={props.sectionData.id}
    name={props.sectionData.name}
    showTitle={props.sectionData.showTitle}
    modifiers={[props.sectionData.id]}
    navData={props.navData}
  >
    <article className='c-section__resume-copy'>
      <section className='c-section__body-copy c-section__body-copy--normal-resume'>
        <h3 className='c-section__sub-heading'>Normal Resume</h3>
        <p>
          Most people are going to want to grab this resume. This resume is a
          standard resume that has my branding and formatting the way it should
          be. The other resume is meant specifically for recruiting agencys so
          that they can paste their logo/branding on it. That obviously doesn't
          apply to most people, so go ahead and grab this one!
        </p>
        <CTAButton
          text='Download Normal Resume'
          href='/aknee-resume.pdf'
          newTab={true}
        />
      </section>
      <section className='c-section__body-copy c-section__body-copy--recruiter-resume'>
        <h3 className='c-section__sub-heading'>Recruiter Resume</h3>
        <p>
          A lot of the time, recruiting agencys like to screenshot your resume,
          throw it in a word doc, and then paste their logo on the top of it.
          The problem is that this often screws up the branding, spacing, and
          print margins on the resume and that drives me absolutely crazy. This
          version has a dedicated space for you to paste your logo/branding.
        </p>
        <CTAButton
          text='Download Recruiter Resume'
          href='/aknee-recruiter-resume.pdf'
          newTab={true}
        />
      </section>
    </article>
  </Section>
);

Resume.propTypes = {
  sectionData: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    showTitle: PropTypes.bool,
    modifiers: PropTypes.array,
    navData: PropTypes.array,
  }),
};

export { Resume };
